package ddts_common;

use ddts_config;

use strict;
#use diagnostics;
use vars qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS $VERSION $basedir $db_base
	 $tmpdir $bindir $status_text %charset @buglist);

use Exporter;
$VERSION=1.00;
@ISA=qw(Exporter);

@EXPORT =qw(
	$status_text
	status
	debug
	warning
	suicide
	get_utf8
        get_utf8_bin
        get_md5_hex
	);

my $inCGI = defined $ENV{SCRIPT_NAME};

use Encode;
use Digest::MD5 qw(md5_hex);
use File::stat;

sub debug   (@) ;
sub suicide (@) ;
sub warning (@) ;
sub status  (@) ;

# Define some constants

# for cmp_email_address
use constant CMPEMPTY   => 1;
use constant NOCMPEMPTY => 0;
use constant EMAILNE    => 1;
use constant EMAILEQ    => 0;

# for list_package_from_translator
#     list_package_from_reviewer
use constant WITHBUGS    => 1;
use constant WITHOUTBUGS => 0;

$status_text="";



# status
#
# write status messages
#
# input:
#   array of lines
sub status (@) {
	my $l;
	my $s;

	$l = length ($1) + 2 if $_[0] =~ /^([ A-Z]*): /; # get offset word

	if ($_[0] =~ /^\s+$/) {				# get offset
		$s = $_[0];
		shift @_;
	}
	$s = $l ? " "x$l : "";

	my $fh = $inCGI ? \*STDERR : \*STDOUT;
	$status_text .= shift(@_)."\n" if $l;
	foreach (@_) {
		next unless defined $_;
		print $fh "debug   status $s$_\n";
		$status_text .= "$s$_\n";
	}
}


# debug
#
# write debug messages
# and align on `=' if possible
#
# input:
#   array of lines
sub debug (@) {
	my $line = (caller(0))[2];			# get calling line
	my $sub  = (caller(1))[3] || '';		# get calling sub name

	$sub =~ s/^main:://;				# remove package name if main
	@_   =  ("") unless @_;				# add an empty item to output caller name if empty

	my @ante;
	foreach (@_) {
		next unless /([^=]*)=(.*)/;
		push @ante, $1;
	}

	my $la=0;
	foreach (@ante) {
		my $l = length($_);
		$la = $la > $l ? $la : $l;
	}
	my $fh = $inCGI ? \*STDERR : \*STDOUT;
	
	foreach (@_) {
		next unless defined $_;
		# print "DEBUG   $_\n";			# uncomment this line and comment all following for previous output format
		print $fh "debug   ($sub\:$line) ";
		if (/([^=]*)=(.*)/) {
			printf $fh "%".$la."s=%s", $1, $2;
		} else {
			print $fh $_;
		}
		print $fh "\n";
	}
}


# suicide
#
# write error message and die
#
# input:
#   array of lines
sub suicide (@) {
	my $line = (caller(0))[2];			# get calling line
	my $sub  = (caller(1))[3] || '';		# get calling sub name

	$sub =~ s/^main:://;				# remove package name if main
	@_   =  ("") unless @_;				# add an empty item to output caller name if empty
	foreach (@_) {
		next unless defined $_;
		print "suicide ($sub\:$line) ";
		print "$_\n";
	}

	exit -1;
}


# warning
#
# write warning message
#
# input:
#   array of lines
sub warning (@) {
	my $line = (caller(0))[2];			# get calling line
	my $sub  = (caller(1))[3] || '';		# get calling sub name

	$sub =~ s/^main:://;				# remove package name if main
	@_   =  ("") unless @_;				# add an empty item to output caller name if empty
	foreach (@_) {
		next unless defined $_;
		print "warning ($sub\:$line) ";
		print "$_\n";
	}
}

# get_utf8_bin
#
# return always binary represantation of utf8 string.
#
# input:
#   utf8 string or octets
sub get_utf8_bin($) {
    my $str = shift;
    return Encode::is_utf8($str) ? Encode::encode("UTF-8", $str, Encode::FB_CROAK | Encode::LEAVE_SRC) : $str;
}

# get_md5_hex
#
# return md5 hash of the input string
#
# input:
#   utf8 string or octets
sub get_md5_hex($) {
    my $str = shift;
    return md5_hex(get_utf8_bin($str));
}

# get_utf8
#
# return valid utf8 string
#
# input:
#   utf8 string or octets
sub get_utf8($) {
    my $str = shift;
    return Encode::is_utf8($str) ? $str : Encode::decode("UTF-8", $str, Encode::FB_CROAK | Encode::LEAVE_SRC);
}


1;
