#!/usr/bin/perl
use lib '/srv/ddtp.debian.org/ddts/bin/';
use diagnostics;
use strict;
use ddts_common;
use Encode qw(decode);
binmode STDOUT, ":utf8";

# If --with-version is provided as first argument, add a Version field in
# the Translation file.
my $WITH_VERSION = 0;
if ($ARGV[0] eq "--with-version") {
	shift @ARGV;
	$WITH_VERSION = 1;
}

my $dists= shift(@ARGV);
my $lang= shift(@ARGV); 

use DBI;

my @DSN = ("DBI:Pg:service=ddtp", "", "");

my $dbh = DBI->connect(@DSN,
    { PrintError => 0,
      RaiseError => 1,
      AutoCommit => 0,
    });

die $DBI::errstr unless $dbh;


sub get_translation {
	my $description_id= shift(@_);
	my $lang= shift(@_);

	my $translation;

	my $sth = $dbh->prepare("SELECT translation FROM translation_tb WHERE description_id=? and language=?");
	$sth->execute($description_id,$lang);
	($translation) = $sth->fetchrow_array;
	if ($translation and (( $translation =~ tr/\n/\n/ )<2)) {
		undef $translation;
	}
	return $translation;
}

sub get_packageinfos {
	my $description_id= shift(@_);

	my $description_md5;

	my $sth = $dbh->prepare("SELECT description_md5 FROM description_tb WHERE description_id=?");
	$sth->execute($description_id);
	($description_md5) = $sth->fetchrow_array;
	return ($description_md5);
}

sub check_utf8 {
	my $str = shift;
	return Encode::is_utf8($str) ? 1 : eval { decode('UTF-8', $str, Encode::FB_CROAK | Encode::LEAVE_SRC); 1 };
}

sub make_translation_file {
	my $tag= shift(@_);

	my %seen_package_and_description_ids;
	my $package;
	my $version;
	my $translation;
	my $description_md5;
	my $description_id;

	open (PACKAGELIST, "<Packages/packagelist-$tag");
	while (<PACKAGELIST>) {
		chomp;
		($package,$version) = split (/ /);
		#print "^$package^$version^";
		my $sth = $dbh->prepare("SELECT description_id FROM description_tb WHERE description_id in (SELECT description_id FROM package_version_tb WHERE package=? and version=?)");
		$sth->execute($package,$version);
		while(($description_id) = $sth->fetchrow_array) {
			#print "  -> $description_id\n";
			unless ($seen_package_and_description_ids{"$package $description_id"}) {
				$translation=get_translation($description_id,$lang);
				($description_md5)= $translation ? get_packageinfos($description_id) : ("");
				if (!check_utf8($translation)) {
					print STDERR "Translation $lang:$package:$description_id:$description_md5 is not valid UTF-8, skipped\n";
					next;
				}
				if ($translation) {
					$translation = get_utf8($translation);
					print "Package: $package\n";
					print "Version: $version\n" if $WITH_VERSION;
					print "Description-md5: $description_md5\n";
					print "Description-$lang: $translation\n";
				}
				$seen_package_and_description_ids{"$package $description_id"}=1;
			}
		}
	}
	close (PACKAGELIST);
}

make_translation_file($dists);
