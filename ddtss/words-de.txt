accessibility	Barrierefreiheit; Zugänglichkeit
account	Konto
address	Adresse (d vs. dd)
agenda	Traktandenliste (für Protokolle)
API	API (f.); Schnittstelle; Programmschnittstelle
applet	Applet; ggf. umschreiben
application	Anwendung; Programm
archive	(»the Debian archive«) das Debian-Archiv
assign	zuordnen; zuweisen
attack	Angriff
audit	(Subst.) Audit (m.), (Verb) etwas einem Audit unterziehen
autoload	automatisch laden
autoloadable	automatisch ladbar
backend	Backend
backup	Sicherheitskopie
binding	Anbindung
language binding	(Sprach-)Anbindung
boot	(Rechner) starten; hochfahren; booten
bootstrap	urladen
BTS	Fehlerdatenbank
buffer overflow	Pufferüberlauf
bug	Fehler
bug report	Fehlerbericht
bug tracking system	Fehlerdatenbank
bylaws	Satzung; Statuten
cache	(Subst.) Zwischenspeicher (allgemein); Cache (technisch), (Verb) zwischen speichern (allgemein); cachen (technisch)
certification mark	Gütesiegel
changelog	Changelog
character	Zeichen; manchmal: Buchstabe
character set	Zeichensatz (z.B. ISO-8859-1 oder Unicode)
click	klicken
client	Client
clone	(Subst.) Nachbau (allgemein); Klon (biol./chem.), (Verb) nachbauen (allgemein); klonen (biol./chem.)
code	(Subst.) Code (Programm-Code); Kodierung, (Verb) kodieren (Kryptographie etc.); programmieren (allgemein)
command	Befehl
command line	Befehlszeile
commit	(Änderungen) übertragen, übergeben, dauerhaft machen
compile	übersetzen; kompilieren
compiler	Compiler
concurrency	Nebenläufigkeit
conffile	Conffile (andere Schreibweisen allerdings als Konfigurationsdatei)
configure	einrichten; konfigurieren; aktivieren
convert	konvertieren; umwandeln (bitte nicht mit »wandeln« übersetzen)
copyright	Copyright
core dump	Speicherabbild
core 	Speicherabbild
cross-compile	cross-kompilieren, Cross-Kompilierung durchführen
cross-compiler	Cross-Compiler
Custom Debian Distribution	Angepasste Debian-Distributionen (bitte beim ersten Auftreten Original stehen lassen und Übersetzung in Klammern dazu)
dead-keys	Tottasten (auf Tastatur)
Debian Account Manager	Verwalter der Debian-Konten
daemon	Daemon; Hintergrundprozess
debugging	Debug- (z.B. debugging library)
deconfigure	umkonfigurieren; deaktivieren (am besten umschreiben, oder durch ein allg. einrichten bzw. konfigurieren ersetzen)
denial of service	Dienstverweigerung; Denial of Service
desktop	die Arbeitsfläche; der Desktop
desktop environment	Arbeitsumgebung
development	Entwicklungs- (z.B. development library, development files)
device	Gerät; Geräteschnittstelle
device file	Gerätedatei
DFSG	Debian-Richtlinien für Freie Software
directory	Verzeichnis
distribute	verteilen; vertreiben
distribution	Distribution
downgrade	das Downgrade; Umschreibung, die Abrüstung
to downgrade	ein Downgrade durchführen; Umschreibung, abrüsten
download	(Subst.) (Datei-)Abruf; Download, (Verb) herunterladen
dummy package	Pseudopaket; leeres Paket (als Umschreibung); Platzhalter; Übergangspaket
dump	Ausgabe, ausgeben, (Datenbank) exportieren
memory dump	Speicherauszug
edit	bearbeiten; verändern; editieren
editor	Editor
e-mail	E-Mail
encoding	Kodierung (z.B. PGP; 8-Bit oder UTF-8)
event	Ereignis (technisch); Veranstaltung (allgemein)
execute	ausführen; ablaufen lassen
exit status	Exit-Status
exploit	Exploit (m.); kann auch umschrieben werden
extract	herausholen, heraus ziehen (z.B. Text aus einer PostScript-Datei)
facilitate	erleichtern; unterstützen
feature	Merkmal; Fähigkeit; Eigenschaft
file	Datei
file system	Dateisystem
flag	Schalter
compiler flag	(Compiler-)Schalter
flag	Markierung, Schalter, Flag
folder	Verzeichnis (Dateisystem); Postfach (E-Mail)
font	Schrift, Zeichensatz
framework	Gerüst; Rahmen(werk)
freeze	(n.) Freeze (m.), (v.)	einfrieren
frontend	Oberfläche (GUI); Frontend (technisch)
gateway	Zugang
GUI	(Grafische) Benutzeroberfläche; Oberfläche; GUI; grafische Benutzungsschnittstelle
handbook	Handbuch
hard link	harter Verweis; harter Link
hardware	Hardware (f.)
header	Header(-Datei)
header file	Header(-Datei)
history	Chronik; Verlauf
command history	Befehlschronik; Befehlsverlauf
home directory	Home-Verzeichnis
home page	Homepage; Web-Seite(n)
hook	Hook (technisch); Einhäng-
host	Rechner; Host
HOWTO	Howto (n.)
icon	Symbol; Icon; Piktogramm; das Ikon; Ikone (da wir uns bislang nicht auf einen gemeinsamen Fachbegriff einigen konnten, bleibt es erstmal der/dem Autorin/Autoren überlassen, welche Übersetzung im jeweiligen Zusammenhang am besten passt)
implementation	Implementierung
input	Eingabe
interface	Schnittstelle (technisch); Oberfläche (GUI)
ISO image	ISO-Image
keymap	Tastenbelegung; (Tasten-)Abbildung
kernel space	Kernel-Bereich; Kernel-Space (tech.)
binding	(Sprach-)Anbindung
language binding	(Sprach-)Anbindung
legal umbrella	gesetzliche Dachorganisation
level	Level (s.) (bei Spielen); sonst Level (m.) (oder Niveau, Ebene)
lightweight	genügsam; leichtgewichtig
library	Bibliothek
link	Verweis, Verknüpfung; Link
local setting	lokale Einstellung
locale	Locale f.
locale-specific	auf die Locale bezogen
localization	Lokalisierung
localize	lokalisieren
locale setting	Einstellung an der Locale; Locale-Einstellung
lock	sperren
log	Protokoll(-datei)
log file	Protokoll(-datei)
mail	E-Mail (sonst: Post)
mail folder	Postfach
mailbox	Postfach
mailing list	Mailingliste
mail user agent	Mailprogramm
maintainer	(Paket-) Betreuer
manual page	Handbuchseite
man page	Handbuchseite
mirror	Spiegel
mount	einhängen/-binden; mounten
mount point	Einhängepunkt
offset	Versatz; Offset m. (tech.), to offset: versetzen
online help	Programmhilfe
on the fly	direkt; im Flug (en passent); kann auch unübersetzt bleiben »On-The-Fly-«
OpenSource	OpenSource
output	Ausgabe
operating system	Betriebssystem; OS (die ausgeschriebene Variante ist im Allgemeinen vorzuziehen)
OS	Betriebssystem; OS (die ausgeschriebene Variante ist im Allgemeinen vorzuziehen)
package	Paket
packaging	Paketierung
panel	Schalttafel, Kontrollfeld, Kontrollzentrum (je nach Art und Größe); (KDE-/GNOME-)Leiste
parse	auswerten, interpretieren; parsen etc.
password	Passwort, Kennwort
patch	Patch (m./n.)
pipe	(Subst.) Weiterleitung; Pipe (w.), (Verb.) weiter leiten
plug-in	Erweiterung; Plugin (n.)
port	(TCP/IP-) Port, (software) port, (Software-) Portierung
position statement	Positionserklärung
powerful	mächtig, leistungsfähig
pre-seed	voreinstellen (Debian Installer)
pre-seed file	Voreinstellungsdatei (Debian Installer)
privilege escalation	Privilegien-Erweiterung
process	(Subst.) Prozess, (Verb)	verarbeiten
profiling	Profilieren; Profilier-
provide	liefert (Abhängigkeit bei Paketen, z.B. »exim4 liefert mail-transport-agent«), enthält
pull in	hereinziehen (automatisches Übertragen von Software oder sonstigen Daten ins System hinein)
purge	vollständig löschen/entfernen
queue	Warteschlange, Queue (Debian spezifische, bspw. NEW-Queue)
QuickGuide	Kurzeinführung
race condition	Race-Condition (technisch), »Umstand des Wettrennens« (zwischen auf eine gemeinsame Ressource zugreifenden Prozessen)
refer	nachsehen; nachschlagen (»schlagen Sie in ... nach«); oft ist auch die Konstruktion »siehe ...« sinnvoll
regular expression	regulärer Ausdruck
regex	regulärer Ausdruck
release	(Subst.) Release; Veröffentlichung, (Verb)	freigeben; veröffentlichen
release critical	release-kritisch
remote	entfernt
render	zeichnen; malen; schreiben; leisten
repository	(Paket-)Depot
resolution	Auflösung (Bildschirm- etc.); Beschluss (organisatorisch)
resolve	ermitteln; auflösen
resource	Ressource (ss vs. s)
return value	Rückgabewert
root compromise	sollte umschrieben werden, z.B »Administratorrechte erlangen«
script	Skript, Pl.: Skripte(n)
scroll	rollen; laufen
search path	Suchpfad
secretary	Schriftführer; (Projekt-)Assistent
section	Bereich (des Debian-Archivs)
security advisory	Sicherheitsgutachten
security alert	Sicherheitswarnung
security policy	Sicherheitsrichtlinie, (Security-)Policy (technisch)
serialization	Serialisierung (Programmierung)
serve	anbieten; dienen
server	Server
set	Satz; Menge; Sammlung
severity	Dringlichkeitsstufe
shared library	Laufzeit-Bibliothek; gemeinsame Bibliothek
shell	Shell
shell prompt	Eingabeaufforderung (der Shell)
shortcut	Tastenkürzel
site	Site; Ort; (auch) Internet-Präsenz oder Web-Auftritt
socket	Socket (technisch)
soft link	symbolischer Verweis; symbolischer Link
software	Software (f.)
source code	Quelltext; Quellen
source	Quelltext; Quellen
SPI BOD	SPI-Vorstand (BOD: Board of Directors)
stack	Stack; Stapelverarbeitungsspeicher
static library	statische Bibliothek
stderr	Standard-Fehlerausgabe; stderr
stdin	Standard-Eingabe; stdin
stdout	Standard-Ausgabe; stdout
string	Zeichenkette
stylesheet	Stilvorlage
suite	Suite
symlink	symbolischer Verweis; symbolischer Link
tag	Etikett; Kennzeichnung; Markierung; Marke; manchmal: Tag
task	Prozess (Betriebssystem); Aufgabe etc. (allgemein)
technical committee	Technischer Ausschuss
technology	Technik; nicht Technologie
technological	technisch
template	Schablone; Vorlage
termination status	Beendigungsstatus
theme	Thema (im Sinne z.B. von »GNOME theme«), Pl.: die Themen
thread	Thread (Betriebssystem), auch Leichtgewichtsprozess, Strang (z.B. Diskussionsstrang)
tool	Programm; Werkzeug; Hilfsprogramm
tool chain	Werkzeugkette, auch Toolchain
trace	verfolgen
trademark	(eingetragenes) Warenzeichen
traversal	Durchlaufen, Sicherheitsloch: Überschreitung
tutorial	einführende Anleitung; auch Tutorial (das)
type cast	Typumwandlung (Programmierung)
type safety	Typsicherheit (Programmierung)
umount	aushängen, Einbindung lösen; unmounten
unmount	aushängen, Einbindung lösen; unmounten
update	die Aktualisierung, to update: aktualisieren; Umschreibung, auf den neuesten Stand
upload	(Subst.) Upload, (Verb) hochladen
upgrade	das Upgrade; Umschreibung, die Aufrüstung, to upgrade: ein Upgrade durchführen; Umschreibung, aufrüsten
upstream	Ursprung-; sollte teilweise umschrieben werden (»the upstream author« => »der Autor dieses Programms«, »ursprünglicher Autor«); manchmal ist auch »Original-« sinnvoll (»upstream sources« => »Original-Quelltexte«)
upstream author	ursprünglicher Autor; Programmautor
upstream maintainer	ursprünglicher Betreuer
uptime	die Uptime; oder Umschreibung über Gesamtlaufzeit, Zeit seit (Neu-)Start
use	Gebrauch
user	Benutzer; Anwender
user interface	Oberfläche; Benutzungsschnittstelle
user space	Benutzer-Bereich; User-Space (tech.)
utility	Programm; Werkzeug; Hilfsprogramm
vendor	Händler; Verkäufer (allgemein); Distributor (Linux)
viewer	Betrachter
web page	Webseite
web site	Webpräsenz, Website
widget	Widget; falls möglich umschreiben
wiki	(das) Wiki
window manager	Fenstermanager
workstation	Arbeitsplatzrechner
wrapper	Hülle; Adapter; (der) Wrapper (tech.)
wrapper class	Hüllenklasse; Adapterklasse; Wrapperklasse (tech.)
wrap	einhüllen; selten: adaptieren
