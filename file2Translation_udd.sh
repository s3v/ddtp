#!/bin/bash -e

. "${DDTP_CONFIG_FILE}"

cd "${DDTP_BASE_DIR}"

# Fetch active langs from database
LANGS=`psql "${DDTP_PSQL_CONNECTION_STRING}" -q -A -t -c "select distinct language from translation_tb where description_id>1"`

DISTS="${DDTP_DISTS_SUPPORTED} ${DDTP_DISTS_UNSTABLE}"

rm -rf Translation_udd

for distribution in $DISTS
do
	sed -e "s/ [^ ][^ ]*$//" < packagelist/$distribution | sort | uniq > Packages/packagelist-$distribution
	for lang in $LANGS
	do
		mkdir -p Translation_udd/dists/$distribution/main/i18n/
		./file2Translation.pl --with-version $distribution $lang | uniq | gzip > Translation_udd/dists/$distribution/main/i18n/Translation-$lang.gz
		echo `date`: create the $distribution/Translation-$lang
	done
	cp packagelist/timestamp packagelist/timestamp.gpg Translation_udd/
	cd Translation_udd
	sha256sum dists/$distribution/main/i18n/Translation-* >> SHA256SUMS
	cd "${DDTP_BASE_DIR}"
done

rm -rf ./www/Translation_udd
cp -a ./Translation_udd/ ./www/
