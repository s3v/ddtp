set terminal png small 

#-# set output "../gnuplot/prioritize-stat.png"
#-# #plot \
#-# #	'prioritize-stat' using 1:2 title "prioritize" with lines
#-# splot \
#-# 	'prioritize-stat' title "prioritize" with lines

set timefmt "%d/%m/%y-%H:%M"
#set nomxtics
set xdata time
set format x "%d.%m\n%Y"
#set logscale y

set output "../gnuplot/ddts-uptodate.png"
plot\
	'stat' using 1:2 title "descriptions in db" with lines,\
	'stat-uptodate' using 1:2 title "uptodate descriptions" with lines

set output "../gnuplot/ddts-stat_begin.png"
plot\
	'stat-hu' using 1:2 title "hu" with lines,\
	'stat-nl' using 1:2 title "nl" with lines,\
	'stat-pl' using 1:2 title "pl" with lines,\
	'stat-sk' using 1:2 title "sk" with lines,\
	'stat-da' using 1:2 title "da" with lines,\
	'stat-uk' using 1:2 title "uk" with lines,\
	'stat-ru' using 1:2 title "ru" with lines,\
	'stat-cs' using 1:2 title "cs" with lines,\
	'stat-sv_SE' using 1:2 title "sv_SE" with lines,\
	'stat-fi' using 1:2 title "fi" with lines

set output "../gnuplot/ddts-stat_bigs.png"
plot [ ] [1:40000] \
	'stat-de' using 1:2 title "de" with lines,\
	'stat-ja' using 1:2 title "ja" with lines,\
	'stat-pt_BR' using 1:2 title "pt_BR" with lines,\
	'stat-it' using 1:2 title "it" with lines,\
	'stat-fr' using 1:2 title "fr" with lines,\
	'stat' using 1:2 title "descriptions in db" with lines,\
	'stat-uptodate' using 1:2 title "uptodate descriptions" with lines

set output "../gnuplot/ddts-stat.png"
plot [ ] [1:10000] \
	'stat-ca' using 1:2 title "ca" with lines,\
	'stat-cs' using 1:2 title "cs" with lines,\
	'stat-de' using 1:2 title "de" with lines,\
	'stat-eo' using 1:2 title "eo" with lines,\
	'stat-es' using 1:2 title "es" with lines,\
	'stat-fi' using 1:2 title "fi" with lines,\
	'stat-fr' using 1:2 title "fr" with lines,\
	'stat-go' using 1:2 title "go" with lines,\
	'stat-hu' using 1:2 title "hu" with lines,\
	'stat-it' using 1:2 title "it" with lines,\
	'stat-ja' using 1:2 title "ja" with lines,\
	'stat-nl' using 1:2 title "nl" with lines,\
	'stat-pl' using 1:2 title "pl" with lines,\
	'stat-pt_AO' using 1:2 title "pt_AO" with lines,\
	'stat-pt_BR' using 1:2 title "pt_BR" with lines,\
	'stat-sk' using 1:2 title "sk" with lines,\
	'stat-da' using 1:2 title "da" with lines,\
	'stat-uk' using 1:2 title "uk" with lines,\
	'stat-ru' using 1:2 title "ru" with lines,\
	'stat-sv_SE' using 1:2 title "sv_SE" with lines,\
	'stat-zh_CN' using 1:2 title "zh_CN" with lines,\
	'stat-zh_TW' using 1:2 title "zh_TW" with lines

#	'stat' using 1:2 title "descriptions in db" with lines,\
#	'stat-uptodate' using 1:2 title "uptodate descriptions" with lines

#set output "../gnuplot/test.png"
#plot\
#	'stat-de' using 1:2 title "1 de" with line 1,\
#	'stat-fr' using 1:2 title "2 fr" with line 2,\
#	'stat-hu' using 1:2 title "3 hu" with line 3,\
#	'stat-it' using 1:2 title "4 it" with line 4,\
#	'stat-ja' using 1:2 title "5 ja" with line 5,\
#	'stat-nl' using 1:2 title "6 nl" with line 6,\
#	'stat-pl' using 1:2 title "7 pl" with line 7,\
#	'stat-pt_BR' using 1:2 title "8 pt_BR" with line 8,\
#	'stat-sk' using 1:2 title "9 sk" with line 9,\
#	'stat-da' using 1:2 title "10 da" with line 10,\
#	'stat-uk' using 1:2 title "11 uk" with line 11,\
#	'stat-ru' using 1:2 title "12 ru" with line 12,\
#	'stat-cs' using 1:2 title "13 cs" with line 13,\
#	'stat-sv_SE' using 1:2 title "14 sv_SE" with line 14,\
#	'stat-uptodate' using 1:2 title "15 uptodate descriptions" with line 15

#-# #reviews
#-# set output "../gnuplot/review-de-stat.png"
#-# plot\
#-# 	'stat-review-de' using 1:3 title "1.reviewer" with lines,\
#-# 	'stat-review-de' using 1:4 title "2.reviewer" with lines,\
#-# 	'stat-review-de' using 1:5 title "3.reviewer" with lines,\
#-# 	'stat-review-de' using 1:6 title "reviewed" with lines
#-# 
#-# set output "../gnuplot/review-fr-stat.png"
#-# plot\
#-# 	'stat-review-fr' using 1:2 title "total" with lines,\
#-# 	'stat-review-fr' using 1:3 title "1.reviewer" with lines,\
#-# 	'stat-review-fr' using 1:4 title "2.reviewer" with lines,\
#-# 	'stat-review-fr' using 1:5 title "3.reviewer" with lines,\
#-# 	'stat-review-fr' using 1:6 title "reviewed" with lines
#-# 
#-# set output "../gnuplot/review-da-stat.png"
#-# plot\
#-# 	'stat-review-da' using 1:2 title "total" with lines,\
#-# 	'stat-review-da' using 1:3 title "1.reviewer" with lines,\
#-# 	'stat-review-da' using 1:4 title "2.reviewer" with lines,\
#-# 	'stat-review-da' using 1:5 title "3.reviewer" with lines,\
#-# 	'stat-review-da' using 1:6 title "reviewed" with lines
#-# 
#-# set output "../gnuplot/review-it-stat.png"
#-# plot\
#-# 	'stat-review-it' using 1:2 title "total" with lines,\
#-# 	'stat-review-it' using 1:3 title "1.reviewer" with lines,\
#-# 	'stat-review-it' using 1:4 title "2.reviewer" with lines,\
#-# 	'stat-review-it' using 1:5 title "3.reviewer" with lines,\
#-# 	'stat-review-it' using 1:6 title "reviewed" with lines
#-# 
#-# set output "../gnuplot/review-fi-stat.png"
#-# plot\
#-# 	'stat-review-fi' using 1:2 title "total" with lines,\
#-# 	'stat-review-fi' using 1:3 title "1.reviewer" with lines,\
#-# 	'stat-review-fi' using 1:4 title "2.reviewer" with lines,\
#-# 	'stat-review-fi' using 1:5 title "3.reviewer" with lines,\
#-# 	'stat-review-fi' using 1:6 title "reviewed" with lines
#-# 
#-# set output "../gnuplot/review-es-stat.png"
#-# plot\
#-# 	'stat-review-es' using 1:2 title "total" with lines,\
#-# 	'stat-review-es' using 1:3 title "1.reviewer" with lines,\
#-# 	'stat-review-es' using 1:4 title "2.reviewer" with lines,\
#-# 	'stat-review-es' using 1:5 title "3.reviewer" with lines,\
#-# 	'stat-review-es' using 1:6 title "reviewed" with lines
#-# 
#-# set output "../gnuplot/review-stat.png"
#-# plot \
#-# 	'stat-review-de' using 1:6 title "de" with lines,\
#-# 	'stat-review-fr' using 1:6 title "fr" with lines,\
#-# 	'stat-review-it' using 1:6 title "it" with lines,\
#-# 	'stat-review-fi' using 1:6 title "fi" with lines,\
#-# 	'stat-review-da' using 1:6 title "da" with lines,\
#-# 	'stat-review-es' using 1:6 title "es" with lines
#-# #
#-# 
#-# set output "../gnuplot/bugs-stat_begin.png"
#-# plot\
#-# 	'bug-close-stat-hu' using 1:2 title "hu" with lines,\
#-# 	'bug-close-stat-ja' using 1:2 title "it" with lines,\
#-# 	'bug-close-stat-nl' using 1:2 title "nl" with lines,\
#-# 	'bug-close-stat-pl' using 1:2 title "pl" with lines,\
#-# 	'bug-close-stat-sk' using 1:2 title "sk" with lines,\
#-# 	'bug-close-stat-da' using 1:2 title "da" with lines,\
#-# 	'bug-close-stat-uk' using 1:2 title "uk" with lines,\
#-# 	'bug-close-stat-ru' using 1:2 title "ru" with lines,\
#-# 	'bug-close-stat-cs' using 1:2 title "cs" with lines,\
#-# 	'bug-close-stat-sv_SE' using 1:2 title "sv_SE" with lines,\
#-# 	'bug-close-stat-fi' using 1:2 title "fi" with lines,\
#-# 	'bug-close-stat-es' using 1:2 title "es" with lines
#-# 
#-# set output "../gnuplot/bugs-stat_bigs.png"
#-# plot\
#-# 	'bug-close-stat-de' using 1:2 title "de" with lines,\
#-# 	'bug-close-stat-ja' using 1:2 title "ja" with lines,\
#-# 	'bug-close-stat-pt_BR' using 1:2 title "pt_BR" with lines,\
#-# 	'bug-close-stat-it' using 1:2 title "it" with lines,\
#-# 	'bug-close-stat-fr' using 1:2 title "fr" with lines
#-# 
#-# set output "../gnuplot/bugs-stat.png"
#-# plot\
#-# 	'bug-close-stat-de' using 1:2 title "de" with lines,\
#-# 	'bug-close-stat-fr' using 1:2 title "fr" with lines,\
#-# 	'bug-close-stat-hu' using 1:2 title "hu" with lines,\
#-# 	'bug-close-stat-it' using 1:2 title "it" with lines,\
#-# 	'bug-close-stat-ja' using 1:2 title "ja" with lines,\
#-# 	'bug-close-stat-nl' using 1:2 title "nl" with lines,\
#-# 	'bug-close-stat-pl' using 1:2 title "pl" with lines,\
#-# 	'bug-close-stat-pt_BR' using 1:2 title "pt_BR" with lines,\
#-# 	'bug-close-stat-sk' using 1:2 title "sk" with lines,\
#-# 	'bug-close-stat-da' using 1:2 title "da" with lines,\
#-# 	'bug-close-stat-uk' using 1:2 title "uk" with lines,\
#-# 	'bug-close-stat-ru' using 1:2 title "ru" with lines,\
#-# 	'bug-close-stat-cs' using 1:2 title "cs" with lines,\
#-# 	'bug-close-stat-sv_SE' using 1:2 title "sv_SE" with lines,\
#-# 	'bug-close-stat-fi' using 1:2 title "fi" with lines
 
set output "../gnuplot/stat-trans-sid-de.png"
plot\
	'stat-trans-sid-de' using 1:2 title "Descriptions" with lines,\
	'stat-trans-sid-de' using 1:3 title "de full trans" with lines,\
	'stat-trans-sid-de' using 1:4 title "de partly trans" with lines

#-# set output "../gnuplot/stat-trans-sarge-main-de.png"
#-# plot\
#-# 	'stat-trans-sarge-main-de' using 1:2 title "Descriptions" with lines,\
#-# 	'stat-trans-sarge-main-de' using 1:3 title "de full trans" with lines,\
#-# 	'stat-trans-sarge-main-de' using 1:4 title "de partly trans" with lines
#-# 
#-# set output "../gnuplot/stat-trans-woody-main-de.png"
#-# plot\
#-# 	'stat-trans-woody-main-de' using 1:2 title "Descriptions" with lines,\
#-# 	'stat-trans-woody-main-de' using 1:3 title "de full trans" with lines,\
#-# 	'stat-trans-woody-main-de' using 1:4 title "de partly trans" with lines
#-# 
#-# set output "../gnuplot/stat-trans-potato-main-de.png"
#-# plot\
#-# 	'stat-trans-potato-main-de' using 1:2 title "Descriptions" with lines,\
#-# 	'stat-trans-potato-main-de' using 1:3 title "de full trans" with lines,\
#-# 	'stat-trans-potato-main-de' using 1:4 title "de partly trans" with lines
 
set output "../gnuplot/stat-trans-sid-es.png"
plot\
	'stat-trans-sid-es' using 1:2 title "Descriptions" with lines,\
	'stat-trans-sid-es' using 1:3 title "es full trans" with lines,\
	'stat-trans-sid-es' using 1:4 title "es partly trans" with lines

#-# set output "../gnuplot/stat-trans-sarge-main-es.png"
#-# plot\
#-# 	'stat-trans-sarge-main-es' using 1:2 title "Descriptions" with lines,\
#-# 	'stat-trans-sarge-main-es' using 1:3 title "es full trans" with lines,\
#-# 	'stat-trans-sarge-main-es' using 1:4 title "es partly trans" with lines
#-# 
#-# set output "../gnuplot/stat-trans-woody-main-es.png"
#-# plot\
#-# 	'stat-trans-woody-main-es' using 1:2 title "Descriptions" with lines,\
#-# 	'stat-trans-woody-main-es' using 1:3 title "es full trans" with lines,\
#-# 	'stat-trans-woody-main-es' using 1:4 title "es partly trans" with lines

set output "../gnuplot/stat-trans-sid-ja.png"
plot\
	'stat-trans-sid-ja' using 1:2 title "Descriptions" with lines,\
	'stat-trans-sid-ja' using 1:3 title "ja full trans" with lines,\
	'stat-trans-sid-ja' using 1:4 title "ja partly trans" with lines

set output "../gnuplot/stat-trans-sid-cs.png"
plot\
	'stat-trans-sid-cs' using 1:2 title "Descriptions" with lines,\
	'stat-trans-sid-cs' using 1:3 title "ja full trans" with lines,\
	'stat-trans-sid-cs' using 1:4 title "ja partly trans" with lines

set output "../gnuplot/stat-trans-sid-pt_BR.png"
plot\
	'stat-trans-sid-pt_BR' using 1:2 title "Descriptions" with lines,\
	'stat-trans-sid-pt_BR' using 1:3 title "ja full trans" with lines,\
	'stat-trans-sid-pt_BR' using 1:4 title "ja partly trans" with lines

set output "../gnuplot/stat-trans-sid-pt_AO.png"
plot\
	'stat-trans-sid-pt_AO' using 1:2 title "Descriptions" with lines,\
	'stat-trans-sid-pt_AO' using 1:3 title "ja full trans" with lines,\
	'stat-trans-sid-pt_AO' using 1:4 title "ja partly trans" with lines

set output "../gnuplot/stat-trans-sid-ko.png"
plot\
	'stat-trans-sid-ko' using 1:2 title "Descriptions" with lines,\
	'stat-trans-sid-ko' using 1:3 title "ja full trans" with lines,\
	'stat-trans-sid-ko' using 1:4 title "ja partly trans" with lines

set output "../gnuplot/stat-trans-sid-fr.png"
plot\
	'stat-trans-sid-fr' using 1:2 title "Descriptions" with lines,\
	'stat-trans-sid-fr' using 1:3 title "fr full trans" with lines,\
	'stat-trans-sid-fr' using 1:4 title "fr partly trans" with lines

#-# set output "../gnuplot/stat-trans-sarge-main-ja.png"
#-# plot\
#-# 	'stat-trans-sarge-main-ja' using 1:2 title "Descriptions" with lines,\
#-# 	'stat-trans-sarge-main-ja' using 1:3 title "de full trans" with lines,\
#-# 	'stat-trans-sarge-main-ja' using 1:4 title "de partly trans" with lines
#-# 
#-# set output "../gnuplot/stat-trans-woody-main-ja.png"
#-# plot\
#-# 	'stat-trans-woody-main-ja' using 1:2 title "Descriptions" with lines,\
#-# 	'stat-trans-woody-main-ja' using 1:3 title "de full trans" with lines,\
#-# 	'stat-trans-woody-main-ja' using 1:4 title "de partly trans" with lines
#-# 
#-# set output "../gnuplot/stat-trans-potato-main-ja.png"
#-# plot\
#-# 	'stat-trans-potato-main-ja' using 1:2 title "Descriptions" with lines,\
#-# 	'stat-trans-potato-main-ja' using 1:3 title "de full trans" with lines,\
#-# 	'stat-trans-potato-main-ja' using 1:4 title "de partly trans" with lines
#-# 
#-# set logscale y
#-# 
#-# set output "../gnuplot/prioritize-stat-de.png"
#-# plot\
#-# 	'prioritize-stat-de' using 1:2 title "desc" with lines,\
#-# 	'prioritize-stat-de' using 1:3 title "trans" with lines,\
#-# 	'prioritize-stat-de' using 1:4 title "rev0" with lines,\
#-# 	'prioritize-stat-de' using 1:5 title "rev1" with lines,\
#-# 	'prioritize-stat-de' using 1:6 title "rev2" with lines
#-# 
#-# set output "../gnuplot/prioritize-stat-de2.png"
#-# plot\
#-# 	'prioritize-stat-de' using 1:2 title "desc" with boxes,\
#-# 	'prioritize-stat-de' using 1:3 title "trans" with boxes,\
#-# 	'prioritize-stat-de' using 1:4 title "rev0" with boxes,\
#-# 	'prioritize-stat-de' using 1:5 title "rev1" with boxes,\
#-# 	'prioritize-stat-de' using 1:6 title "rev2" with boxes
#-# 
#-# set output "../gnuplot/prioritize-stat-fr.png"
#-# plot\
#-# 	'prioritize-stat-fr' using 1:2 title "desc" with lines,\
#-# 	'prioritize-stat-fr' using 1:3 title "trans" with lines,\
#-# 	'prioritize-stat-fr' using 1:4 title "rev0" with lines,\
#-# 	'prioritize-stat-fr' using 1:5 title "rev1" with lines,\
#-# 	'prioritize-stat-fr' using 1:6 title "rev2" with lines
#-# 
#-# set nologscale y
#-# 
#-# set ytics nomirror
#-# set ylabel "sum bugs"
#-# set y2range []
#-# set y2tics autofreq
#-# set y2label "open bugs"
#-# set output "../gnuplot/bugs-all.png"
#-# plot\
#-# 	'bug-stat' using 1:2 title "sum bugs" with lines,\
#-# 	'bug-close-stat' using 1:2 axes x1y2 title "open bugs" with lines
#-# 
